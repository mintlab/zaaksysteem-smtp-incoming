# Build using: docker build -t "zs_smtp_incoming:latest" .
FROM python:3-alpine

COPY microsmtpd.py /microsmtpd.py
RUN pip install requests
CMD [ "python", "/microsmtpd.py" ]
