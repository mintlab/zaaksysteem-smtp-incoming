#! /usr/bin/env python3
"""
Zaaksysteem SMTP server - incoming mail
"""
import asyncio
import boto3
import botocore
import logging
import os
import pathlib
import redis
import requests
import ssl
import sys

from aiosmtpd.controller import Controller, SMTP
from logging.config import fileConfig


# Use a custom logger object instead of the root
log = logging.getLogger(__name__)


class IncomingSMTPServer:
    """
    A simple SMTP server that delivers all incoming mail to configured
    Zaaksysteem instances, based on the recipient address.
    """

    def __init__(self, redis_conn, redis_key, route_file):
        super().__init__()

        self.redis = redis_conn
        self.redis_key = redis_key
        self.route_file = route_file
        self.update_routes()

    def update_routes(self):
        self.routes = {}
        if self.route_file is None:
            log.warning("No route configuration is found")
            return

        log.info('Loading route configuration from {}'.format(self.route_file))

        with open(self.route_file) as routes:
            for line in routes:
                suffix, host = line.partition("=")[::2]
                self.routes[suffix.strip()] = host.strip()

    async def handle_RCPT(
        self, server, session, envelope, address, rcpt_options
    ):
        
        route = self.lookup_route(address)
        if route is None:
            log.error('Route not found for {}.'.format(address),
                      extra={
                          "status": "ERROR",
                          "status_code": 550,
                          "mail_to": address,
                          "mail_from": envelope.mail_from
                      }
            )
            return "550 We do not accept mail for that domain."

        envelope.rcpt_tos.append(address)

        return "250 OK"

    async def handle_DATA(self, server, session, envelope):
        

        mailfrom = envelope.mail_from

        rcpttos = envelope.rcpt_tos

        data = envelope.content
        
        if len(rcpttos) > 1:
            log.error("More than one RCPT TO in SMTP transaction (only one allowed).",
                      extra={
                          "status": "ERROR",
                          "status_code": 450,
                          "mail_to": rcpttos,
                          "mail_from": mailfrom
                      }
            )
            return (
                "450 Requested mail action not taken: only one recipient "
                + " is allowed per message"
            )
        
        recipient = rcpttos[0]
        log.info('Message received from {} to {}'.format(mailfrom, recipient),
                 extra={
                     "mail_from": mailfrom,
                     "mail_to": recipient
                 }
        )

        route = self.lookup_route(recipient)
        if route is None:  # Should never happen -- see handle_RCPT
            log.error('Route not found for {}.'.format(recipient),
                      extra={"status": "ERROR",
                             "status_code": 550,
                             "mail-in.recipient": recipient}
            )
            return "550 Mailbox unavailable"

        return self.post_message(route, data)

    def post_message(self, recipient_domain, data):

        destination_url = "https://{}/api/mail/intake".format(recipient_domain)

        # Create a "file upload" multi-part part.
        files = {
            "message": (
                "incoming-email.mbox",
                data,
                "application/octet-stream",
            )
        }

        # Post it to
        res = requests.post(destination_url, files=files)
        request_id = res.headers.get("zs-req-id", "<no request-id>")

        try:
            res.raise_for_status()
        except requests.exceptions.HTTPError:
            log.error('Error while posting message to URL {}: {} (request id: {})'.format(
                destination_url, res.status_code, request_id),
                extra={
                    "status": "ERROR",
                    "status_code": 451,
                    "req.request_uri": destination_url,
                    "req.status": res.status_code,
                    "req.request_id": request_id
                }
            )
            return "451 Requested action aborted: error in processing"

        log.info('Posted message to {} with request id {}'.format(recipient_domain, request_id),
                 extra={
                     "status": "OK",
                     "status_code": 250,
                     "req.request_uri": destination_url,
                     "req.status": res.status_code,
                     "req.request_id": request_id
                 }
        )
        return "250 OK"

    def lookup_route(self, recipient):

        recipient_domain = recipient.partition("@")[-1]

        if recipient_domain in self.routes:
            # Backwards compatible mode for "old" mail hostnames
            # (XX.api.zaaksysteem.nl)
            # Mapped from a static config file.
            log.info('Found mail domain {} in legacy configuration'.format(recipient_domain))
            return self.routes[recipient_domain]

        try:

            self.redis.ping()

            if self.redis.sismember(self.redis_key, recipient_domain):
                log.info('Found mail domain {} in Redis'.format(recipient_domain))
                return recipient_domain

            log.info('Mail domain {} not found in Redis'.format(recipient_domain))

        except redis.exceptions.ConnectionError as error:
            log.error("Could not connect to Redis: '{}'".format(error))

        return


def get_config():
    return {
        "logging_config": os.environ.get("LOGGING_CONFIG", None),
        "mode": "incoming",
        "port": int(os.environ.get("LISTEN_PORT", 25)),
        "redis_host": os.environ.get("REDIS_HOST", "localhost"),
        "redis_key": os.environ.get("REDIS_KEY", "saas:hosts"),
        "redis_port": int(os.environ.get("REDIS_PORT", 6379)),
        "route_config": os.environ.get("ROUTE_CONFIG", "/tmp/routes.conf"),
        "s3_bucket_name": os.environ.get("S3_BUCKET_NAME"),
        "s3_routes_path": os.environ.get("S3_ROUTES_PATH"),
        "tls_privkey": os.environ.get("TLS_PRIVKEY_PATH", "/opt/xxllnc/zaakgericht/smtp-relay.key"),
        "tls_pubkey": os.environ.get("TLS_PUBKEY_PATH", "/opt/xxllnc/zaakgericht/smtp-relay.pem"),
        "redis_ssl": bool(os.environ.get("REDIS_SSL", False)),
    }


def init_tls_context(config):
    privkey = pathlib.Path(config["tls_privkey"])
    pubkey = pathlib.Path(config["tls_pubkey"])

    if not privkey.is_file():
        return None

    if not pubkey.is_file():
        return None

    log.info('Configuring SSL context')

    context = ssl.create_default_context(ssl.Purpose.CLIENT_AUTH)
    context.load_cert_chain(pubkey, privkey)

    return context


async def amain(loop, config):
    redis_conn = redis.StrictRedis(
        host=config["redis_host"], port=config["redis_port"], db=0, ssl=config["redis_ssl"]
    )

    smtp_handler = IncomingSMTPServer(
        redis_conn, config["redis_key"], config["route_config"]
    )

    tls_context = init_tls_context(config)

    controller = Controller(
        smtp_handler,
        hostname="",
        port=config["port"],
        ident="Zaaksysteem SMTP-Incoming",
        server_hostname=os.environ.get("MAIL_HOSTNAME", "unknown"),
        tls_context=tls_context,
    )

    controller.start()


def main():
    config = get_config()
    s3     = boto3.resource("s3")

    if config["logging_config"]:
        fileConfig(config["logging_config"])
    else:
        logging.basicConfig(
            level=logging.INFO, format="%(asctime)s %(levelname)s:%(message)s"
        )

    # Retrieve routes.conf from S3
    if config["s3_bucket_name"]:
        try:
            s3.Bucket(config["s3_bucket_name"]).download_file(
                config["s3_routes_path"],
                config["route_config"]
            )

            log.info(f"Downloading routes.conf from {config['s3_bucket_name']}/{config['s3_routes_path']}")

            with open(config["route_config"]) as f:
                for line in f:
                    log.info(f"Found route {line}")

        except botocore.exceptions.ClientError as e:
            if e.response["Error"]["Code"] == "404":
                log.error(f"Failed downloading routes.conf from {config['s3_bucket_name']}/{config['s3_routes_path']}: File not found")
            else:
                raise

    loop = asyncio.get_event_loop()
    loop.create_task(amain(loop=loop, config=config))

    try:
        loop.run_forever()
    except KeyboardInterrupt:
        pass


if __name__ == "__main__":
    main()
