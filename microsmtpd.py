import asyncore
import logging
import requests
import signal
import sys
from os import environ
from smtpd import SMTPServer

class IncomingSMTPServer(SMTPServer):
    """
    A simple SMTP server that delivers all incoming mail to the configured
    Zaaksysteem instances, based on the recipient address.
    """

    def set_route_file(self, route_file):
        self.route_file = route_file

    def update_routes(self):
        logging.info('Reloading route configuration')
        self.routes = {}

        with open(self.route_file) as routes:
            for line in routes:
                suffix, url = line.partition('=')[::2]
                self.routes[suffix.strip()] = url.strip()

    def process_message(self, peer, mailfrom, rcpttos, data, **kwargs):
        if len(rcpttos) > 1:
            logging.error('More than one RCPT TO in SMTP transaction (only one allowed).')
            return '450 Requested mail action not taken: only one recipient is allowed per message'

        recipient = rcpttos[0]
        logging.info( 'Message received from "{}" to "{}"'.format(mailfrom, recipient) )

        recipient_domain = recipient.partition('@')[-1]

        if recipient_domain not in self.routes:
            logging.error('Route not found for "{}".'.format(recipient))
            return '450 Requested mail action not taken: mailbox unavailable'

        files = { 'message': ('', data, 'application/octet-stream') }
        r = requests.post(self.routes[recipient_domain], files=files)

        try:
            r.raise_for_status()
        except:
            logging.error(
                'Error while posting message to URL "{}": "{}"'.format(
                    self.routes[recipient_domain],
                    r.status_code
                )
            )
            return '451 Requested action aborted: error in processing'

        return


def get_config():
    cfg = {
        'mode': 'incoming',
        'port': 8025,
        'route_file': '/etc/smtp_routes',
    }

    if 'SMTPD_PORT' in environ:
        cfg['port'] = int(environ['SMTPD_PORT'])

    if 'SMTPD_ROUTE_FILE' in environ:
        cfg['route_file'] = environ['SMTPD_ROUTE_FILE']

    return cfg

def signal_handler(signum, frame):
    print("Signal {} received. Exiting gracefully.".format(signum))
    sys.exit(0)

def main():
    logging.basicConfig(level=logging.INFO)
    config = get_config()

    s = IncomingSMTPServer(('0.0.0.0', config['port']), None)
    #, decode_data=False)
    s.set_route_file(config['route_file'])
    s.update_routes()

    # Reload configuration on HUP
    signal.signal(signal.SIGHUP, lambda signum, frame: s.update_routes())

    # Die quietly when asked
    signal.signal(signal.SIGINT, signal_handler)
    signal.signal(signal.SIGTERM, signal_handler)

    while True:
        asyncore.loop()

if __name__ == '__main__':
    main()
