aiosmtpd~=1.4
redis~=5.1
hiredis~=2.4
requests~=2.32
python-json-logger~=2.0
botocore~=1.35
boto3~=1.35
