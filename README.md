# README #

This is a small Python 3 based SMTP daemon that sends all received email messages to the configured HTTP(S) URL.

The URL the message is sent to is determined by the configuration file. Each line in this file should have the following format:

`domain.tld=https://some.url`

Where "domain.tld" is the domain part of the recipient address, and "https://some.url" is the URL to send it to.

The message will be sent as a file upload in a multipart/form-data POST message, in a field named "message".