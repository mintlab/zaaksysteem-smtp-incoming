# zaaksysteem-smtp-incoming #

This is a small Python 3 "smtpd" based SMTP daemon that posts received email
messages to a configured Zaaksysteem instance.

The URL the message is sent to is determined by checking Redis, then if the key
is not found, the domain is looked up in a configuration file with a legacy
"email host" to "https host" mapping. If the domain is not found in either of
those places, a hard error (5xx) is returned to the sending party.

1. Redis. If the hostname exists in the configured Redis key (which should be a
   "set"), it will be used to deliver mail to.

2. Using the "legacy" configuration file. Each line in this file should have
   the following format:

   `domain.tld=zaaksysteem.hostname`

   Where `domain.tld` is the domain part of the recipient address, and
   `zaaksysteem.hostname` is the hostname of the Zaaksysteem instance to
   deliver the email to.

The email message will be sent to the configured Zaaksysteem instance as a file
upload in a multipart/form-data POST message, in a field named `message`.

## Configuration ##

The daemon is configured with environment variables:

* `LISTEN_PORT`
  The port on which to listen for incoming connections. Defaults to `25`

* `REDIS_HOST`
  The hostname of the Redis-server to use. Defaults to `localhost`
  
* `REDIS_PORT`
  Port to use to connect to Redis. Defaults to `6379`

* `REDIS_KEY`
  Redis key to query. This should be a set (`SADD`, `SMEMBERS`, etc). Defaults to `saas:hosts`

* `ROUTE_CONFIG`
  Location of the route configuration file. This is optional

* `FLUENT_HOST`
  The hostname of the FluentD-server to use. This is optional

* `FLUENT_PORT`
  Port to use to connect to FluentD. Defaults to `24224`


## Building a new image ##

`docker build -t registry.gitlab.com/zaaksysteem/zaaksysteem-smtp-incoming .`
`docker push registry.gitlab.com/zaaksysteem/zaaksysteem-smtp-incoming`

## Docker compose ##

This daemon can be fired up with docker-compose.
